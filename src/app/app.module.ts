import { MaterializeModule } from 'angular2-materialize';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';


import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RoomComponent } from './components/room/room.component';
import { BathroomComponent } from './components/bathroom/bathroom.component';
import { KitchenComponent } from './components/kitchen/kitchen.component';
import { LivingRoomComponent } from './components/living-room/living-room.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    RoomComponent,
    BathroomComponent,
    KitchenComponent,
    LivingRoomComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterializeModule,
    RouterModule.forRoot([
      {
        path: '',
        component: DashboardComponent,
        data: {state: 'home'}
      },
      {
        path: 'room',
        component: RoomComponent,
        data: {state: 'room'}
      },
      {
        path: 'bathroom',
        component: BathroomComponent,
        data: {state: 'bathroom'}
      },
      {
        path: 'kitchen',
        component: KitchenComponent,
        data: {state: 'kitchen'}
      },
      {
        path: 'livingRoom',
        component: LivingRoomComponent,
        data: {state: 'livingRoom'}
      }
    ])
  ],
  providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
