import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-living-room',
  templateUrl: './living-room.component.html',
  styleUrls: ['./living-room.component.css']
})
export class LivingRoomComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.cargar();
  }
  cargar() {
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/encendidoclima.png" alt="" id="Encendido-img">');
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/apagadoclima.png" alt="" id="Apagado-img">');

    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender() {
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar() {
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender2() {
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar2() {
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender3() {
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/encendidoclima.png" alt="" id="Encendido-img">');
  }
  apagar3() {
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/apagadoclima.png" alt="" id="Apagado-img">');
  }
  encender4() {
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar4() {
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }

}
