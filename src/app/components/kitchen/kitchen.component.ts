import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-kitchen',
  templateUrl: './kitchen.component.html',
  styleUrls: ['./kitchen.component.css']
})
export class KitchenComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.cargar();
  }

  cargar() {
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender() {
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar() {
    $('#imagenes1 img:last-child').remove();
    $('#imagenes1').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender2() {
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar2() {
    $('#imagenes2 img:last-child').remove();
    $('#imagenes2').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender3() {
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar3() {
    $('#imagenes3 img:last-child').remove();
    $('#imagenes3').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }
  encender4() {
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/encendido.png" alt="" id="Encendido-img">');
  }
  apagar4() {
    $('#imagenes4 img:last-child').remove();
    $('#imagenes4').append('<img src="../../../assets/images/apagado.png" alt="" id="Apagado-img">');
  }

}
